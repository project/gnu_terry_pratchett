<?php

namespace Drupal\gnu_terry_pratchett\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class XClacksSubscriber implements EventSubscriberInterface {


  /**
   * Sets the X-Clacks-Overhead header on successful responses.
   *
   * @param ResponseEvent $event
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();

    // Set the X-Clacks-Overhead HTTP header to keep the legacy of Terry
    // Pratchett alive forever.
    $response->headers->set('X-Clacks-Overhead', 'GNU Terry Pratchett', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('onRespond');
    return $events;
  }

}
